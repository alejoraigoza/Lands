﻿

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Lands.Infrastructure
{

    using ViewModels;
    
    public  partial class InstanceLocator : ContentPage
	{
        #region Properties
        public MainViewModel Main
        {
            get;
            set;
        }
        #endregion

        #region constructors
        public InstanceLocator ()
		{
			this.Main =new MainViewModel();
		}
        #endregion
    }
}