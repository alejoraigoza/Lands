﻿
namespace Lands.ViewModels
{
    using Xamarin.Forms;
    using Xamarin.Forms.Xaml;

    
	public partial class MainViewModel
	{
        #region ViewModels

        public LoginViewModel Login
        {
         get;
         set;
        }
        #endregion

        #region Constructors
        public MainViewModel ()

		{
		this.Login = new LoginViewModel();
		}
        #endregion
    }

}